#include <stdio.h>
#include <unistd.h>

void main(int argc, char *argv[], char *envp[]) {

    printf("%d аргументов:\n", argc);

    for (int i = 0; i < argc; i++) {
        printf("%s ", argv[i]);
    }

    puts("");

    for (; (*envp) != NULL; envp++) {
        printf("%s\n", *envp);
    }
}
