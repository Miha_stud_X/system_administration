#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "thread.h"

int a = 0;


void *mythread(void *dummy)
{

	pthread_t mythid;
	mythid = pthread_self();

	a = a+1;

	printf("Thread %ld, Calculation result = %d\n", mythid, a);

	return NULL;
}

int take_threads(){

	pthread_t thid[2], mythid;
	int result[2];

	for (int i = 0; i<2; i++){
		result[i] = pthread_create( &thid[i], (pthread_attr_t *)NULL, mythread, NULL);
		if (result[i] != 0) {
			printf("Error on thread create %d, return value = %d\n", i+1, result[i]);
			exit(-1);
		}
		printf("Thread %d created, thid = %ld\n", i+1, thid[i]);
	}

	mythid = pthread_self();

	a = a+1;

	for(int i = 0; i<2; i++) {
		pthread_join(thid[i], (void **) NULL);
	}

	printf("Thread %ld, Calculation result = %d\n", mythid, a);

	return 0;
}