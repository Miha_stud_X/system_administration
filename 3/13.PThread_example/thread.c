#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "thread.h"

int a = 0;


void *mythread(void *dummy)
{

	pthread_t mythid;
	mythid = pthread_self();

	a = a+1;

	printf("Thread %ld, Calculation result = %d\n", mythid, a);

	return NULL;
}

int take_threads(){

	pthread_t thid, mythid;
	int result;

	result = pthread_create( &thid, (pthread_attr_t *)NULL, mythread, NULL);

	if(result != 0){
		printf ("Error on thread create, return value = %d\n", result);
		exit(-1);
	}

	printf("Thread created, thid = %ld\n", thid);

	mythid = pthread_self();

	a = a+1;

	printf("Thread %ld, Calculation result = %d\n", mythid, a);

	pthread_join(thid, (void **)NULL);

	return 0;
}

/*
void *func(void *ptc){
	int *ptc1 = ptc;
	printf("%d\n",*ptc1);
	return 0;
}

int take_threads(){
	
	pthread_t tid[5];
	int mass[5] = {1,2,3,4,5};
	
	for (int i = 0; i<5; i++){
		pthread_create(&tid[i], NULL, func, &mass[i]);
	}
	
	for (int i = 0; i<5; i++){
		pthread_join(tid[i], NULL);
	}
	
	return 0;
}
*/
