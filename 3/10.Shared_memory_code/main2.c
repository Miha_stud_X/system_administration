#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(){
    int *array; /* Указатель на разделяемую память */
    int shmid; /* IPC дескриптор для области разделяемой памяти */
    int flag_new = 1; /* Флаг необходимости инициализации элементов массива */
    char pathname[] = "main.c"; /* Имя файла, использующееся для генерации ключа.
                                * Файл с таким именем должен существовать в текущей директории */
    key_t key; /* IPC ключ */

/* Генерируем IPC ключ из имени файла 06-1a.c в текущей директории и номера экземпляра области разделяемой памяти 0 */
    if((key = ftok(pathname,0)) < 0){
        printf("Can\'t generate key\n");
        exit(-1);
    }

    if((shmid = shmget(key, 1*sizeof(int), 0)) < 0){
        printf("Can\'t find shared memory\n");
        exit(-1);
    }

/* Пытаемся отобразить разделяемую память в адресное пространство текущего процесса.
 * Обратите внимание на то, что для правильного сравнения мы явно преобразовываем значение -1 к указателю на целое. */
    if((array = (int *)shmat(shmid, NULL, 0)) == (int *)(-1)){
        printf("Can't attach shared memory\n");
        exit(-1);
    }

    for(int i = 0; i<array[0]; i++){
        putchar(array[i+1]);
    }
    putchar('\n');

    if(shmdt(array) < 0){
        printf("Can't detach shared memory\n");
        exit(-1);
    }

    if(shmctl(shmid, IPC_RMID, (struct shmid_ds *)array) < 0){
        printf("Can't remove shared memory\n");
        exit(-1);
    }

    return 0;
}