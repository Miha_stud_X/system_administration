#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int count_char_in_file(const char* filename)
{
    int ch, count_ch = 0;
    int *mass_ch;
    FILE *file = fopen(filename, "r");
    while (!feof(file) && !ferror(file))
    {
        ch = getc(file);
        if (ch != EOF)
        {
            count_ch++;
        }
    }
    return count_ch;
}


void cat(const char* filename, int count, int *array)
{
    array[0]=count;
    int ch;
    int *mass_ch;
    FILE *file = fopen(filename, "r");
    for(int i = 0; i<count; i++){
        ch = getc(file);
        if (ch != EOF)
        {
            array[i+1]=ch;
        }
    }
}

int main() {
    int *array; /* Указатель на разделяемую память */
    int shmid; /* IPC дескриптор для области разделяемой памяти */
    char pathname[] = "main.c"; /* Имя файла, использующееся для генерации ключа.
                                * Файл с таким именем должен существовать в текущей директории */
    key_t key; /* IPC ключ */
    int count = count_char_in_file(pathname);

/* Генерируем IPC ключ из имени файла main.c в текущей директории и номера экземпляра области разделяемой памяти 0 */
    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can\'t generate key\n");
        exit(-1);
    }

/* Пытаемся эксклюзивно создать разделяемую память для сгенерированного ключа, т.е. если для этого ключа она уже
 * существует системный вызов вернет отрицательное значение.
 * Размер памяти определяем как размер массива из 3-х целых переменных,
 * права доступа 0666 - чтение и запись разрешены для всех */
    if ((shmid = shmget(key, (count+1) * sizeof(int), 0666 | IPC_CREAT | IPC_EXCL)) < 0) {
        /* В случае возникновения ошибки пытаемся определить: возникла ли она из-за того, что сегмент разделяемой памяти
        * уже существует или по другой причине */
        if (errno != EEXIST) {
            /* Если по другой причине - прекращаем работу */
            printf("Can\'t create shared memory\n");
            exit(-1);
        } else {
            puts("Shared memory already created!");
            return 1;
        }
    }

/* Пытаемся отобразить разделяемую память в адресное пространство текущего процесса.
 * Обратите внимание на то, что для правильного сравнения мы явно преобразовываем значение -1 к указателю на целое.*/
    if ((array = (int *) shmat(shmid, NULL, 0)) == (int *) (-1)) {
        printf("Can't attach shared memory\n");
        exit(-1);
    }

    cat(pathname, count, array);
    puts("Program 1 recorded code text in shared memory ");

    if (shmdt(array) < 0) {
        printf("Can't detach shared memory\n");
        exit(-1);
    }
    return 0;
}