#include <wait.h>
#include "pipe_link.h"

int main(int argc, char **argv) {

    int pid;
    int fd[2];
    char fds[5];
    char str[str_size + 1] = "Hello, world!";

    if (pipe(fd) < 0) {

        printf("Can\'t create first pipe\n");

        exit(-1);
    }

    sprintf(fds, "%d", fd[0]);

    if ((pid = fork()) == 0) {

        execl("pipe_link_2", "pipe_link_2", fds, NULL);

    } else {

        close(fd[0]);

        if (write(fd[1], str, strlen(str)) != strlen(str)) {

            printf("\nCan\'t write all string\n");

            exit(-1);
        }

        wait(NULL);

        close(fd[1]);
    }
    return 1;
}
