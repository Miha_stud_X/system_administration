#include "pipe_link.h"

int main(int argc, char *argv[]) {

    char str[str_size + 1];

    if (argc != 2) {

        printf("%d: %s\n", argc, argv[0]);
        puts("error!");

        return 0;
    }

    int nfd = atoi(argv[1]);

    if (read(nfd, str, str_size) != str_size) {

        printf("\nCan\'t read all string\n");

        exit(-1);
    }

    printf("\nПолучена строка: %s\n", str);

    close(nfd);

    return 1;
}