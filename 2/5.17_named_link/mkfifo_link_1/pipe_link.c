#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "pipe_link.h"

#define STR_SIZE 255
#define NAME_FILE "./../file_test"

int take_link(){

	int fd, len;
    char buf[STR_SIZE];

    (void)umask(0);

    mkfifo(NAME_FILE, 0777);

    if ((fd = open(NAME_FILE, O_RDONLY))<0){
        puts("Не удалось открыть файл FIFO!");
        exit(-1);
    }
    printf("Канал создан через файл \"%s\"\n", NAME_FILE);

    while(1){
        memset(buf, '\0', STR_SIZE);
        if ((len = read(fd, buf, STR_SIZE-1)) <= 0 ) {
            close(fd);
            remove(NAME_FILE);
            return 0;
        }
        printf("Входящее сообщение (%d): %s\n", len, buf);
    }
}
