#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "pipe_link.h"

#define STR_SIZE 255
#define NAME_FILE "./../file_test"

int take_link(){

    int fd;
    char buf[]="Это тест именованного канала, созданного через MKFIFO";

    if((fd = open(NAME_FILE, O_RDWR))<0){
        puts("Не удалось открыть файл FIFO!");
        exit(-1);
    }
    printf("Канал создан через файл \"%s\"\n", NAME_FILE);
    if (write(fd, buf, STR_SIZE-1)!=STR_SIZE-1){
        printf("Не удалось записать всю строку!\n");
        exit(-1);
    }
    close(fd);
    return 0;
}
