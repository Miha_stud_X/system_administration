#include "pipe_link.h"

#define str_size 14

void take_link() {

    int fd1[2], fd2[2], result;

    if (pipe(fd1) < 0) {

        printf("Can\'t create first pipe\n");
        exit(-1);
    }

    if (pipe(fd2) < 0) {

        printf("Can\'t create second pipe\n");
        exit(-1);
    }

    if ((result = fork()) < 0) {

        printf("Parent: Can\'t fork child\n");
        exit(-1);

    } else if (result > 0) {

        parent(fd1, fd2);

    } else {

        child(fd1, fd2);
    }
}

void parent(int fd1[2], int fd2[2]) {

    char resstring[str_size];
    size_t size;

    close(fd1[0]);
    close(fd2[1]);

    size = write(fd1[1], "Hello, child!", str_size);

    if (size != str_size) {
        printf("Parent: Can\'t write all string\n");
        exit(-1);
    }
    size = read(fd2[0], resstring, 14);

    if (size < 0) {
        printf("Parent: Can\'t read string\n");
        exit(-1);
    }
    printf("Child: %s\n", resstring);

    close(fd1[1]);
    close(fd2[0]);
}

void child(int fd1[2], int fd2[2]) {

    char resstring[str_size];
    size_t size;

    close(fd1[1]);
    close(fd2[0]);

    size = write(fd2[1], "Hello, parent", str_size);

    if (size != str_size) {
        printf("Child: Can\'t write all string\n");
        exit(-1);
    }

    size = read(fd1[0], resstring, 14);
    if (size < 0) {
        printf("Child: Can\'t read string\n");
        exit(-1);
    }

    printf("Parent: %s\n", resstring);

    close(fd1[0]);
    close(fd2[1]);
}
