#include "pipe_link.h"

int take_link() {

    int fd[2], n = 0;

    if (pipe(fd) < 0) {

        printf("Can\'t create pipe\n");
        exit(-1);
    }

    while (1) {

        write(fd[1], "1", 1);
        n++;

        printf("Размер: %d\n", n);
    }
}
