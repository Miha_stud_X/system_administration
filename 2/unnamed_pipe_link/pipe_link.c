#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "pipe_link.h"

#define str_size 5

int take_link(){
	
	int pid;
	int fd[2];
	char str[str_size+1] = "Hello";
	pipe(fd);
	if ((pid = fork())==0){
		read(fd[1], str, str_size);
		printf("\nПолучена строка: %s\n", str);
	}else{
		write(fd[0], str, strlen(str));
	}
	return 1;
}
