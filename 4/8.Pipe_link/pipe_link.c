#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>

#define STR_SIZE 14

/*
 * ДЛЯ ТОГО, ЧТОБЫ УВИДЕТЬ КАК РАБОТАЕТ БЕЗ СИНХРОНИЗАЦИИ ДОСТАТОЧНО ЗАКОММЕНТИРОВАТЬ СТРОКИ:
 * 67-77
 * 105-115
*/

int main() {

    int fd[2], result;
    size_t size;
    char resstring[STR_SIZE];
    char str1[] = "Hello, child!";
    char str2[] = "Hello, father";

    int semid; /* IPC дескриптор для массива IPC семафоров */
    char pathname[] = "pipe_link.c"; /* Имя файла, использующееся для генерации ключа.*/
    key_t key; /* IPC ключ */
    struct sembuf mybuf; /* Структура для задания операции над семафором */

   if((key = ftok(pathname,0)) < 0){
        printf("Can\'t generate key\n");
        exit(-1);
    }

    /* Пытаемся создать массив семафоров с правами доступа read & write для всех пользователей */
    if((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0){
        printf("Can\'t get semid\n");
        exit(-1);
    }

    /* Попытаемся создать pipe */
    if (pipe(fd) < 0) {
        /* Если создать pipe не удалось, печатаем об этом сообщение и прекращаем работу */
        printf("Can\'t create pipe\n");
        exit(-1);
    }

    /* Порождаем новый процесс */
    result = fork();

    if (result < 0) {
        /* Если создать процесс не удалось, сообщаем об этом и завершаем работу */
        printf("Can\'t fork child\n");
        exit(-1);
    } else {
        if (result > 0) {//если создать процесс удалось
            /* Мы находимся в родительском процессе*/

            //сначала отец пишет
            size = write(fd[1], str1, STR_SIZE);
            if (size != STR_SIZE) {
                /* Если записалось меньшее количество байт, сообщаем об ошибке и завершаем работу */
                printf("Parent: can\'t write all string\n");
                exit(-1);
            }
            /* Закрываем входной поток данных и ожидаем ответа*/
            close(fd[1]);

            /* Используем массив семафоров для синхронизации
             * Выполним операцию D(semid1,1) для нашего массива семафоров.*/
            mybuf.sem_op = -1;
            mybuf.sem_flg = 0;
            mybuf.sem_num = 0;

            if(semop(semid, &mybuf, 1) < 0){
                printf("Can\'t wait for condition\n");
                exit(-1);
            }
            //----------

            //теперь отец читает ответ
            size = read(fd[0], resstring, 14);
            if (size < 0) {
                /* Если прочитать не смогли, сообщаем об ошибке и завершаем работу */
                printf("Parent: can\'t read string\n");
                exit(-1);
            }
            printf("Parent received a message: %s\n", resstring);//выводим прочитанное
            close(fd[0]);
            printf("Parent exit\n");
        } else {
            /* Мы находимся в порожденном процессе, который будет получать информацию от процесса-родителя и ответит.
            * Он унаследовал от родителя таблицу открытых файлов и, зная файловые дескрипторы, соответствующие pip'у,
            * может его использовать.*/

            //сначала сын читает
            size = read(fd[0], resstring, 14);
            if (size < 0) {
                /* Если прочитать не смогли, сообщаем об ошибке и завершаем работу */
                printf("Child: can\'t read string\n");
                exit(-1);
            }
            printf("Child received a message: %s\n", resstring);//выводим прочитанное
            close(fd[0]);

            /* Используем массив семафоров для синхронизации
             * Выполним операцию A(semid1,1) для нашего массива семафоров.*/
            mybuf.sem_op = 1;
            mybuf.sem_flg = 0;
            mybuf.sem_num = 0;

            if(semop(semid, &mybuf, 1) < 0){
                printf("Can\'t wait for condition\n");
                exit(-1);
            }
            //----------

            //теперь сын отвечает
            size = write(fd[1], str2, STR_SIZE);
            if (size != STR_SIZE) {
                /* Если записалось меньшее количество байт, сообщаем об ошибке и завершаем работу */
                printf("Child: can\'t write all string\n");
                exit(-1);
            }
            close(fd[1]);
            printf("Child exit\n");
        }
    }
    return 0;
}